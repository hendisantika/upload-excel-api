package com.hendisantika.uploadexcelapi.repository;

import com.hendisantika.uploadexcelapi.model.Employee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

/**
 * Created by IntelliJ IDEA.
 * Project : upload-excel-api
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2018-12-02
 * Time: 07:05
 * To change this template use File | Settings | File Templates.
 */

@RepositoryRestResource
public interface EmployeeRepository extends JpaRepository<Employee, Integer> {
}
