package com.hendisantika.uploadexcelapi.controller;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

/**
 * Created by IntelliJ IDEA.
 * Project : upload-excel-api
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2018-12-02
 * Time: 07:09
 * To change this template use File | Settings | File Templates.
 */
@ControllerAdvice
public class UploadExcelExceptionHandler {
    @ExceptionHandler(Exception.class)
    public String handleError1(Exception e,
                               RedirectAttributes redirectAttributes) {
        redirectAttributes.addFlashAttribute("message", e.getCause()
                .getMessage());
        return "redirect:/uploadStatus";

    }
}