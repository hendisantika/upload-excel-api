# Upload Excel API

This application helps to upload an Excel file to Server Directory and will save Excel Record to DB then Display All records in UI page

Step :

1. First i created One Dummy Directory so when i upload my File it will directly store in that Directory Step.

2. When i click view Links it will fetch all records From Excel then will Display in UI page and All Excel Data like Employee Details will be stored in DataBase.

Run this project by this command : `mvn clean spring-boot:run`

### Screenshot

Upload Page

![Upload Page](img/upload.png "Upload Page")

Success Page

![Success Page](img/success.png "Success Page")

Details Page

![Details Page](img/list.png "Details Page")